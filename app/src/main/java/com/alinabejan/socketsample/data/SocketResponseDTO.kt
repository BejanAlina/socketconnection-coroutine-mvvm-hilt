package com.alinabejan.socketsample.data

data class SocketResponse<out T>(
    val status: SocketStatus,
    val data: T?,
    val throwable: Throwable? = null
) {
    companion object {
        fun <T> open(): SocketResponse<T> {
            return SocketResponse(SocketStatus.OPEN, null)
        }

        fun <T> success(data: T?): SocketResponse<T> {
            return SocketResponse(SocketStatus.SUCCESS, data, null)
        }

        fun <T> error(msg: Throwable?, data: T? = null): SocketResponse<T> {
            return SocketResponse(SocketStatus.ERROR, data, msg)
        }

    }
}

enum class SocketStatus {
    OPEN,
    SUCCESS,
    ERROR
}

class SocketAbortedException : Exception()
