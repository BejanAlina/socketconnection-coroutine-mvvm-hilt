package com.alinabejan.socketsample.di

import com.alinabejan.socketsample.repository.SocketRepository
import com.alinabejan.socketsample.repository.SocketRepositoryImpl
import com.alinabejan.socketsample.service.SocketApiService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import okhttp3.OkHttpClient
import java.util.concurrent.TimeUnit
import javax.inject.Qualifier
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
class SocketModule {

    @SocketClient
    @Singleton
    @Provides
    fun providesSocketOkHttpClient(
    ): OkHttpClient = OkHttpClient.Builder()
        .readTimeout(10, TimeUnit.SECONDS)
        .connectTimeout(10, TimeUnit.SECONDS)
        .build()

    @Singleton
    @Provides
    fun providesSocketRepository(
        @SocketClient client: OkHttpClient,
        @IoDispatcher dispatcher: CoroutineDispatcher
    ): SocketRepository {
        return SocketRepositoryImpl(SocketApiService(dispatcher, client))
    }

    @IoDispatcher
    @Singleton
    @Provides
    fun providesIoDispatcher(): CoroutineDispatcher = Dispatchers.IO
}

@Retention(AnnotationRetention.BINARY)
@Qualifier
annotation class IoDispatcher

@Retention(AnnotationRetention.BINARY)
@Qualifier
annotation class SocketClient