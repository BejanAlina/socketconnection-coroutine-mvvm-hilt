package com.alinabejan.socketsample.model

import okio.ByteString

data class SocketResponseModel(
    val text: String? = null,
    val byteString: ByteString? = null,
    val exception: Throwable? = null
)