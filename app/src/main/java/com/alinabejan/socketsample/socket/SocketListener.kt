package com.alinabejan.socketsample.socket

import com.alinabejan.socketsample.data.SocketAbortedException
import com.alinabejan.socketsample.data.SocketResponse
import com.alinabejan.socketsample.data.SocketResponseDTO
import com.alinabejan.socketsample.model.SocketResponseModel
import com.alinabejan.socketsample.service.SocketApiService.Companion.NORMAL_CLOSURE_STATUS
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch
import okhttp3.Response
import okhttp3.WebSocket
import okhttp3.WebSocketListener
import timber.log.Timber


class SocketListener(ioDispatcher: CoroutineDispatcher) : WebSocketListener() {
    private val _socketEventFlow = MutableSharedFlow<SocketResponse<SocketResponseModel>>()
    private val coroutineScope = CoroutineScope(
        ioDispatcher
    )
    val socketEventFlow = _socketEventFlow.asSharedFlow()
    var isOpen: Boolean = false

    override fun onOpen(webSocket: WebSocket, response: Response) {
        Timber.i("Socket connection: onOpen: $response")
        isOpen = true
        coroutineScope.launch {
            _socketEventFlow.emit(SocketResponse.open())
        }
    }

    override fun onMessage(webSocket: WebSocket, text: String) {
        Timber.i("Socket connection: onMessage: $text")
        coroutineScope.launch {
            _socketEventFlow.emit(SocketResponse.success(SocketResponseModel(text)))
        }
    }

    override fun onClosing(webSocket: WebSocket, code: Int, reason: String) {
        Timber.i("Socket connection: onClosing: $code $reason")
        coroutineScope.launch {
            _socketEventFlow.emit(SocketResponse.error(SocketAbortedException()))
        }
        webSocket.close(NORMAL_CLOSURE_STATUS, null)
        isOpen = false
    }

    override fun onFailure(webSocket: WebSocket, t: Throwable, response: Response?) {
        Timber.i("Socket connection: onFailure: $t")
        coroutineScope.launch {
            _socketEventFlow.emit(SocketResponse.error(t))
        }
        isOpen = false
    }
}
