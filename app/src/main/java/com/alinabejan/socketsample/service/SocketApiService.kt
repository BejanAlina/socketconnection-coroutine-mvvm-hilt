package com.alinabejan.socketsample.service

import com.alinabejan.socketsample.BuildConfig
import com.alinabejan.socketsample.data.SocketRequestDTO
import com.alinabejan.socketsample.data.SocketResponse
import com.alinabejan.socketsample.di.IoDispatcher
import com.alinabejan.socketsample.model.SocketResponseModel
import com.alinabejan.socketsample.socket.SocketListener
import com.google.gson.Gson
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.SharedFlow
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.WebSocket
import timber.log.Timber

class SocketApiService(
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher,
    private val okHttpClient: OkHttpClient
) {
    private var webSocket: WebSocket? = null
    private var socketListener: SocketListener? = null
    private val gson = Gson()

    /**
     *Change delay time according to your socket server
     */
    private suspend fun startSocket(socketListener: SocketListener, delay: Boolean = false) {
        if (delay) {
            delay(1000)
        }
        createWebSocket(socketListener)
    }

    private fun createWebSocket(socketListener: SocketListener) {
        this@SocketApiService.socketListener = socketListener
        webSocket = okHttpClient.newWebSocket(
            Request.Builder().url(BuildConfig.BASE_SOCKET_URL).build(),
            socketListener
        )
    }

    suspend fun startSocket(): SharedFlow<SocketResponse<SocketResponseModel>> {
        val stopSocket = webSocket != null || socketListener != null
        if (stopSocket) {
            stopSocket()
        }
        return with(SocketListener(ioDispatcher)) {
            startSocket(this, delay = stopSocket)
            this@with.socketEventFlow
        }
    }

    fun isConnected(): Boolean = socketListener?.isOpen ?: false

    fun stopSocket() {
        try {
            webSocket?.close(NORMAL_CLOSURE_STATUS, "Connection closed")
            webSocket = null
            socketListener = null
            okHttpClient.dispatcher.cancelAll()
            okHttpClient.connectionPool.evictAll()
        } catch (ex: Exception) {

        }
    }

    /**
     * some ws servers require a specific PING message
     * if you server does not use that, just use  .pingInterval() on your OkHttpClient
     */
    suspend fun startPing() {
        while (socketListener?.isOpen == true) {
            delay(PING_INTERVAL_MS)
            sendPing()
        }
    }

    private fun sendPing() {
        if (socketListener?.isOpen == true) {
            val socketRequestDTO = SocketRequestDTO("PING")
            sendMessage(gson.toJson(socketRequestDTO))
        }
    }

    private fun sendMessage(SocketRequestDTO: String) {
        Timber.i("Socket connection: sendMessage: $SocketRequestDTO")
        webSocket?.send(SocketRequestDTO)
    }

    fun sendSomething(message: String) {
        val socketRequestDTO = SocketRequestDTO(message)
        sendMessage(gson.toJson(socketRequestDTO))
    }

    companion object {
        const val NORMAL_CLOSURE_STATUS = 1000
        const val PING_INTERVAL_MS = 30000L
    }
}
