package com.alinabejan.socketsample.repository

import com.alinabejan.socketsample.data.SocketResponse
import com.alinabejan.socketsample.model.SocketResponseModel
import com.alinabejan.socketsample.service.SocketApiService
import kotlinx.coroutines.flow.SharedFlow

class SocketRepositoryImpl(
    private val scoreSocketApiService: SocketApiService
) : SocketRepository {

    override suspend fun startSocket(): SharedFlow<SocketResponse<SocketResponseModel>> =
        scoreSocketApiService.startSocket()

    override fun closeSocket() {
        scoreSocketApiService.stopSocket()
    }

    override fun isConnected(): Boolean {
        return scoreSocketApiService.isConnected()
    }

    override suspend fun sendSomething(message: String) {
        scoreSocketApiService.sendSomething(message)
    }

    override suspend fun startPing() {
        scoreSocketApiService.startPing()
    }

}

interface SocketRepository {
    suspend fun startSocket(): SharedFlow<SocketResponse<SocketResponseModel>>

    fun closeSocket()

    fun isConnected(): Boolean

    suspend fun sendSomething(message: String)

    suspend fun startPing()

}