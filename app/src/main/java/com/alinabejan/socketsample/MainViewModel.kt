package com.alinabejan.socketsample

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.alinabejan.socketsample.data.SocketStatus
import com.alinabejan.socketsample.model.SocketResponseModel
import com.alinabejan.socketsample.repository.SocketRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val socketRepository: SocketRepository
) : ViewModel() {

    init {
        connectToSocket()
    }

    private fun connectToSocket() {
        viewModelScope.launch {
            try {
                socketRepository.startSocket().collect {
                    when (it.status) {
                        SocketStatus.OPEN -> {
                            viewModelScope.launch { socketRepository.startPing() }
                        }
                        SocketStatus.SUCCESS -> {
                            decodeWSResponse(it.data)
                        }
                        SocketStatus.ERROR -> {
                            // onSocketError(it.throwable)
                        }
                    }
                }
            } catch (ex: Exception) {
                //onSocketError(ex)
            }
        }
    }

    private fun decodeWSResponse(data: SocketResponseModel?) {
        TODO("Not yet implemented")
    }

    fun sendSomeMessage() {
        if (socketRepository.isConnected()) {
            viewModelScope.launch {
                socketRepository.sendSomething("Hey! I'm connected to your socket")
            }
        }
    }

}